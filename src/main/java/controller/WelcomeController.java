package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/WelcomeController")
public class WelcomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Cookie[] cookie = request.getCookies();
		for(Cookie c:cookie) {
			if(c.getName().equals("key")) {
				if(c.getValue().equals("")) {
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/Login.jsp");
					rd.forward(request, response);
				}
				else {
					request.setAttribute("key2", c.getValue());
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/Welcome.jsp");
					rd.forward(request, response);
				}
			}
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie[] cookie = request.getCookies();
		for(Cookie c:cookie) {
			if(c.getName().equals("key")) {
				c.setMaxAge(0);
				response.addCookie(c);
			}
		}
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/Login.jsp");
		rd.forward(request, response);
	}

}
